:- use_rendering(table,
		 [header(dom('Národnosť', 'Farba', 'Zviera', 'Nápoj', 'Cigarety'))]).

susedia(D1, D2, Ulica) :- nextto(D1, D2, Ulica); nextto(D2, D1, Ulica).
nalavo(D1, D2, Ulica) :- nextto(D1, D2, Ulica).

majitelRyby(Majitel) :-
	napovedy(Domy),
	member(dom(Majitel, _, ryba, _, _), Domy).   

%dom(farba, narodnost, zviera, napoj, cigarety)
napovedy(Domy) :-
    length(Domy, 5),
    % Angličan žije v červenom dome.
    member(dom(anglican, cervena, _, _, _), Domy),
    % Švéd chová psy.
    member(dom(sved, _, pes, _, _), Domy),
    % Dán pije čaj.
    member(dom(dan, _, _, caj, _), Domy),
    % Zelený dom je hneď naľavo od bieleho.
    nalavo(dom(_, zeleny, _, _, _), dom(_, biely, _, _, _), Domy),
    % Obyvateľ zeleného domu pije kávu.
    member(dom(_, zeleny, _, kava, _), Domy),
    % Ten, kto fajčí cigarety Pall Mall, chová vtáky.
    member(dom(_, _, vtak, _, pall_mall), Domy),
    % Obyvateľ žltého domu fajčí cigarety Dunhill.
    member(dom(_, zlty, _, _, dunhill), Domy),
    % Ten, kto žije v prostrednom dome, pije mlieko.
    nth1(3, Domy, dom(_, _, _, mlieko, _)),
    % Nór žije v prvom dome.
    nth1(1, Domy, dom(nor, _, _, _, _)),
    % Ten, kto fajčí cigarety Blends, žije vedľa chovateľa mačiek.
    susedia(dom(_, _, _, _, blends), dom(_, _, macka, _, _), Domy),
    % Chovateľ koní žije vedľa toho, kto fajčí cigarety Dunhill.
    susedia(dom(_, _, _, _, dunhill), dom(_, _, kon, _, _), Domy),
    % Ten, kto fajčí cigarety Blue Master, pije pivo.
    member(dom(_, _, _, pivo, blue_master), Domy),
    % Nemec fajčí cigarety Prince.
    member(dom(nemec, _, _, _, prince), Domy),
    % Nór žije vedľa modrého domu.
    susedia(dom(nor, _, _, _, _), dom(_, modra, _, _, _), Domy),
    % Sused toho, kto fajčí cigarety Blends, pije vodu.
    susedia(dom(_, _, _, _, blends), dom(_, _, _, voda, _), Domy),
    % Niekto chová rybu.
    member(dom(_, _, ryba, _, _), Domy). 